%{!?_httpd_apxs: %{expand: %%global _httpd_apxs %%{_sbindir}/apxs}}
%{!?_httpd_mmn: %{expand: %%global _httpd_mmn %%(cat %{_includedir}/httpd/.mmn 2>/dev/null || echo 0-0)}}
%{!?_httpd_confdir:    %{expand: %%global _httpd_confdir    %%{_sysconfdir}/httpd/conf.d}}
%{!?_httpd_modconfdir: %{expand: %%global _httpd_modconfdir %%{_sysconfdir}/httpd/conf.d}}
%{!?_httpd_moddir:    %{expand: %%global _httpd_moddir    %%{_libdir}/httpd/modules}}
%global sphinxbin %{_bindir}/sphinx-build-3
Name:                mod_wsgi
Version:             4.6.4
Release:             2
Summary:             A WSGI interface for Python web applications in Apache
License:             ASL 2.0
URL:                 https://modwsgi.readthedocs.io/
Source0:             https://github.com/GrahamDumpleton/mod_wsgi/archive/%{version}.tar.gz#/mod_wsgi-%{version}.tar.gz
Source1:             wsgi-python3.conf
Patch1:              mod_wsgi-4.5.20-exports.patch
Patch2:              Use-official-APIs-for-accessing-interpreter-list.patch
Patch3:              Changed-functions-to-pre-post-actions-when-forking.patch
BuildRequires:       httpd-devel gcc
%{?filter_provides_in: %filter_provides_in %{_httpd_moddir}/.*\.so$}
%{?filter_setup}
%description
The mod_wsgi adapter is an Apache module that provides a WSGI compliant
interface for hosting Python based web applications within Apache. The
adapter is written completely in C code against the Apache C runtime and
for hosting WSGI applications within Apache has a lower overhead than using
existing WSGI adapters for mod_python or CGI.

%package -n python3-%{name}
Summary:             %summary
Requires:            httpd-mmn = %{_httpd_mmn}
BuildRequires:       python3-devel, python3-sphinx
Provides:            mod_wsgi = %{version}-%{release}
Provides:            mod_wsgi%{?_isa} = %{version}-%{release}
Obsoletes:           mod_wsgi < %{version}-%{release}
%description -n python3-%{name}
The mod_wsgi adapter is an Apache module that provides a WSGI compliant
interface for hosting Python based web applications within Apache. The
adapter is written completely in C code against the Apache C runtime and
for hosting WSGI applications within Apache has a lower overhead than using
existing WSGI adapters for mod_python or CGI.


%prep
%autosetup n %{name}-%{version} -p1

%build
make -C docs html SPHINXBUILD=%{sphinxbin}
export LDFLAGS="$RPM_LD_FLAGS -L%{_libdir}"
export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
mkdir py3build/
cp -R * py3build/ || :
pushd py3build
%configure --enable-shared --with-apxs=%{_httpd_apxs} --with-python=python3
make %{?_smp_mflags}
%py3_build
popd

%install
pushd py3build
make install DESTDIR=$RPM_BUILD_ROOT LIBEXECDIR=%{_httpd_moddir}
mv  $RPM_BUILD_ROOT%{_httpd_moddir}/mod_wsgi{,_python3}.so
install -d -m 755 $RPM_BUILD_ROOT%{_httpd_modconfdir}
install -p -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_httpd_modconfdir}/10-wsgi-python3.conf

%py3_install
mv $RPM_BUILD_ROOT%{_bindir}/mod_wsgi-express{,-3}
popd

%files -n python3-%{name}
%license LICENSE
%doc CREDITS.rst README.rst
%config(noreplace) %{_httpd_modconfdir}/*wsgi-python3.conf
%{_httpd_moddir}/mod_wsgi_python3.so
%{python3_sitearch}/mod_wsgi-*.egg-info
%{python3_sitearch}/mod_wsgi
%{_bindir}/mod_wsgi-express-3

%changelog
* Sat Feb 27 2021 zhaorenhai <zhaorenhai@hotmail.com> - 4.6.4-2
- Add configure file

* Thu Nov 19 2020 huanghaitao <huanghaitao8@huawei.com> - 4.6.4-1
- package init
